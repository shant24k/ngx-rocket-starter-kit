import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NzDemoHorizontalMenuComponent } from './nz-demo-horizontal-menu.component';

describe('NzDemoHorizontalMenuComponent', () => {
  let component: NzDemoHorizontalMenuComponent;
  let fixture: ComponentFixture<NzDemoHorizontalMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NzDemoHorizontalMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NzDemoHorizontalMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
